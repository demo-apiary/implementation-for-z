var express = require('express')
var app = express()

app.get('/orders', function (req, res) {
  res.json([
    {customer_name: 'Adam', details: 'socks', quantity: 42},
    {customer_name: 'Honza', details: 'coffee', quantity: 2},
  ])
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
